package itis.bookshelf.cfg.database;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Connection {
    private static Connection DBconnection = new Connection();
    private static java.sql.Connection connection;

    private Connection() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/Bookshelf",
                    "postgres",
                    "postgres"
            );
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static java.sql.Connection getConnection() {
        return connection;
    }
}