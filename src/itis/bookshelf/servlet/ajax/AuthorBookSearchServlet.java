package itis.bookshelf.servlet.ajax;

import itis.bookshelf.entity.Author;
import itis.bookshelf.entity.Book;
import itis.bookshelf.service.entity.AuthorService;
import itis.bookshelf.service.entity.BookService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AuthorBookSearchServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/json");
        response.setCharacterEncoding("UTF-8");
        String q = request.getParameter("q");
        JSONObject jo = new JSONObject();
        JSONArray names = new JSONArray();
        JSONArray ids = new JSONArray();
        try {
            List<Book> books = BookService.search(q);
            List<Author> authors = AuthorService.search(q);
            if (books != null && !books.isEmpty()) {
                for (Book o : books) {
                    ids.put("/book?id=" + o.getId());
                    names.put(o.getName());
                }
            }
            if (authors != null && !authors.isEmpty()) {
                for (Author o : authors) {
                    ids.put("/author?id=" + o.getId());
                    names.put(o.getName());
                }
            }
            jo.put("names", names);
            jo.put("ids", ids);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        response.getWriter().print(jo.toString());
        response.getWriter().close();
    }

}
