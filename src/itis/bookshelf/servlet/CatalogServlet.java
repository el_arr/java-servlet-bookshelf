package itis.bookshelf.servlet;

import itis.bookshelf.cfg.view.Freemarker;
import itis.bookshelf.entity.Author;
import itis.bookshelf.entity.BookAuthorGenre;
import itis.bookshelf.entity.Genre;
import itis.bookshelf.service.entity.AuthorService;
import itis.bookshelf.service.entity.BookAuthorGenreService;
import itis.bookshelf.service.entity.BookAuthorService;
import itis.bookshelf.service.entity.GenreService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CatalogServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        Map<String, Object> root = new HashMap<>();

        if (request.getSession().getAttribute("current-user") != null)
            root.put("logged", true);
        else
            root.put("logged", false);

        List<Genre> genres = GenreService.getAll();
        if (genres != null) {
            List<List<Genre>> genreList = new ArrayList<>();
            int i = 0;
            for (i = 0; i < genres.size() % 6 + 1; i++) {
                genreList.add(new ArrayList<>());
            }
            i = 0;
            for (Genre g : genres) {
                if (genreList.get(i).size() < 6) {
                    genreList.get(i).add(g);
                } else {
                    i++;
                    genreList.get(i).add(g);
                }
            }
            root.put("genreList", genreList);
        }

        String[] genreCheckBoxes = request.getParameterValues("genre");
        String query = request.getParameter("query");
        if ((query == null || "".equals(query)) && genreCheckBoxes == null) {
            root.put("books_authors", BookAuthorService.getAll());
            root.put("queried", false);
        } else {
            List<BookAuthorGenre> list = BookAuthorGenreService.getAll();
            List<BookAuthorGenre> result = new ArrayList<>();
            if ("".equals(query)) {
                for (String genreId : genreCheckBoxes) {
                    for (BookAuthorGenre bag : list) {
                        if (bag.getGenre().getId().equals(Integer.valueOf(genreId))) {
                            result.add(bag);
                        }
                    }
                }
            } else {
                Author author = AuthorService.searchOne(query);
                if (list != null && author != null) {
                    if (genreCheckBoxes == null || genreCheckBoxes.length == 0) {
                        for (BookAuthorGenre bag : list) {
                            if (author.getId() == bag.getAuthor().getId()) {
                                result.add(bag);
                            }
                        }
                    } else {
                        for (String genreId : genreCheckBoxes) {
                            for (BookAuthorGenre bag : list) {
                                if (bag.getGenre().getId().equals(Integer.valueOf(genreId)) &&
                                        author.getId() == bag.getAuthor().getId()) {
                                    result.add(bag);
                                }
                            }
                        }
                    }
                }
            }
            root.put("books_authors_genres", result);
            root.put("queried", true);
        }
        Freemarker.start("books.ftl", root, response);
    }

}
