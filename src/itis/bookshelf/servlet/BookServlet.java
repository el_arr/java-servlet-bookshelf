package itis.bookshelf.servlet;

import itis.bookshelf.cfg.view.Freemarker;
import itis.bookshelf.entity.BookAuthor;
import itis.bookshelf.entity.Review;
import itis.bookshelf.service.entity.BookAuthorService;
import itis.bookshelf.service.entity.ReviewService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        Map<String, Object> root = new HashMap<>();
        if (request.getSession().getAttribute("current-user") != null)
            root.put("logged", true);
        else
            root.put("logged", false);
        Integer id = Integer.valueOf(request.getParameter("id"));
        if (id != null) {
            BookAuthor ba = BookAuthorService.getByBookId(id);
            root.put("book_author", ba);
            List<Review> reviews = ReviewService.getByBookId(id);
            root.put("reviews", reviews);
        }
        Freemarker.start("books_id=x.ftl", root, response);
    }

}
