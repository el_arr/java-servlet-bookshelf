package itis.bookshelf.service.entity;

import itis.bookshelf.entity.Genre;
import itis.bookshelf.repository.Repository;
import itis.bookshelf.repository.impl.GenreRepositoryImpl;

import java.util.List;

public class GenreService {
    private static Repository<Genre> genreRepository = new GenreRepositoryImpl();

    public static List<Genre> getAll() {
        return genreRepository.getByQuery(
                "SELECT * FROM genre"
        );
    }

}
