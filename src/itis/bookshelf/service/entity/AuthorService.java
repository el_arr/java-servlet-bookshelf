package itis.bookshelf.service.entity;

import itis.bookshelf.entity.Author;
import itis.bookshelf.repository.Repository;
import itis.bookshelf.repository.impl.AuthorRepositoryImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthorService {
    private static Repository<Author> authorRepository = new AuthorRepositoryImpl();

    public static Author getById(Integer id) {
        List<Author> authors = authorRepository.getByQuery(
                "SELECT * " +
                        "FROM author " +
                        "WHERE id =" + id + ";"
        );
        if (authors != null)
            return authors.get(0);
        return null;
    }

    public static List<Author> search(String q) {
        if (!"".equals(q)) {
            Pattern p = Pattern.compile(
                    ".*" + q.toLowerCase() + ".*"
            );
            Matcher m;
            List<Author> list = new ArrayList<>();
            for (Author a : authorRepository.getByQuery("SELECT * FROM author")) {
                m = p.matcher(
                        a.getName().toLowerCase()
                );
                if (m.find()) {
                    list.add(a);
                }
            }
            return list;
        }
        return null;
    }

    public static Author searchOne(String q) {
        if (!"".equals(q)) {
            Pattern p = Pattern.compile(
                    ".*" + q.toLowerCase() + ".*"
            );
            Matcher m;
            for (Author a : authorRepository.getByQuery("SELECT * FROM author")) {
                m = p.matcher(
                        a.getName().toLowerCase()
                );
                if (m.find()) {
                    return a;
                }
            }
        }
        return null;
    }

}
