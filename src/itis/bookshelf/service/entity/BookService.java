package itis.bookshelf.service.entity;

import itis.bookshelf.entity.Book;
import itis.bookshelf.repository.Repository;
import itis.bookshelf.repository.impl.BookRepositoryImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookService {
    private static Repository<Book> bookRepository = new BookRepositoryImpl();

    public static Book getById(Integer id) {
        List<Book> books = bookRepository.getByQuery(
                "SELECT * " +
                        "FROM book " +
                        "WHERE id =" + id + ";"
        );
        if (books != null)
            return books.get(0);
        return null;
    }

    public static List<Book> search(String q) {
        if (!"".equals(q)) {
            Pattern p = Pattern.compile(
                    ".*" + q.toLowerCase() + ".*"
            );
            Matcher m;
            List<Book> list = new ArrayList<>();
            for (Book b : bookRepository.getByQuery("SELECT * FROM book")) {
                String s = b.getName();
                m = p.matcher(s.toLowerCase());
                if (m.find()) {
                    list.add(
                            new Book(s, b.getId())
                    );
                }
            }
            return list;
        }
        return null;
    }

    public static List<Book> getAll() {
        return bookRepository.getByQuery(
                "SELECT * FROM book"
        );
    }

}
