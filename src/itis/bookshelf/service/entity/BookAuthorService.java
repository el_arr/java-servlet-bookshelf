package itis.bookshelf.service.entity;

import itis.bookshelf.entity.BookAuthor;
import itis.bookshelf.repository.Repository;
import itis.bookshelf.repository.impl.BookAuthorRepositoryImpl;

import java.util.List;

public class BookAuthorService {
    private static Repository<BookAuthor> bookAuthorRepository = new BookAuthorRepositoryImpl();

    public static List<BookAuthor> getAll() {
        return bookAuthorRepository.getByQuery(
                "SELECT book.name as book_name, description, cover_path, topic_id, year_pub, " +
                        "pages, intro_path, book.language, is_popular, is_new, book.id as book_id," +
                        "author.name as author_name, biography, img_path, author.id as author_id " +
                        " FROM book_author " +
                        "INNER JOIN book ON book_author.book_id = book.id " +
                        "INNER JOIN author ON book_author.author_id = author.id"
        );
    }

    public static BookAuthor getByBookId(Integer id) {
        List<BookAuthor> booksAuthors = bookAuthorRepository.getByQuery(
                "SELECT" +
                        "  book.name   AS book_name," +
                        "  description," +
                        "  cover_path," +
                        "  topic_id," +
                        "  year_pub," +
                        "  pages," +
                        "  intro_path," +
                        "  book.language," +
                        "  is_popular," +
                        "  is_new," +
                        "  book.id     AS book_id," +
                        "  author.name AS author_name," +
                        "  biography," +
                        "  img_path," +
                        "  author.id   AS author_id " +
                        "FROM book_author " +
                        "  INNER JOIN book ON book_author.book_id = book.id" +
                        "  INNER JOIN author ON book_author.author_id = author.id " +
                        "WHERE book_id =" + id + ";"
        );
        if (booksAuthors != null)
            return booksAuthors.get(0);
        return null;
    }

    public static List<BookAuthor> getByAuthorId(Integer id) {
        return bookAuthorRepository.getByQuery(
                "SELECT" +
                        "  book.name   AS book_name," +
                        "  description," +
                        "  cover_path," +
                        "  topic_id," +
                        "  year_pub," +
                        "  pages," +
                        "  intro_path," +
                        "  book.language," +
                        "  is_popular," +
                        "  is_new," +
                        "  book.id     AS book_id," +
                        "  author.name AS author_name," +
                        "  biography," +
                        "  img_path," +
                        "  author.id   AS author_id " +
                        "FROM book_author " +
                        "  INNER JOIN book ON book_author.book_id = book.id" +
                        "  INNER JOIN author ON book_author.author_id = author.id " +
                        "WHERE author_id =" + id + ";"
        );
    }

}
