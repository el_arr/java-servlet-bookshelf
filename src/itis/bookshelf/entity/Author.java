package itis.bookshelf.entity;

public class Author {
    private String name;
    private String biography;
    private String img_path;
    private int id;

    public Author(String name, String biography, String img_path, int id) {
        this.name = name;
        this.biography = biography;
        this.img_path = img_path;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getBiography() {
        return biography;
    }

    public String getImg_path() {
        return img_path;
    }

    public int getId() {
        return id;
    }
}
