package itis.bookshelf.entity;

public class Image {
    private String img_path;
    private int slider_id;
    private int id;

    public Image(String img_path, int slider_id, int id) {
        this.img_path = img_path;
        this.slider_id = slider_id;
        this.id = id;
    }
}
