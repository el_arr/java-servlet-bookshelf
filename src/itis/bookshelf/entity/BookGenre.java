package itis.bookshelf.entity;

public class BookGenre {
    private Book book;
    private Genre genre;

    public BookGenre(Book book, Genre genre) {
        this.book = book;
        this.genre = genre;
    }

    public Book getBook() {
        return book;
    }

    public Genre getGenre() {
        return genre;
    }
}
