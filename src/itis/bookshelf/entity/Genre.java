package itis.bookshelf.entity;

public class Genre {
    private String name;
    private Integer id;

    public Genre(String name, Integer id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }
}
