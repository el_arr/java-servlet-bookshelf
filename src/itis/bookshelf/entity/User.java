package itis.bookshelf.entity;

public class User {
    private String username;
    private String login;
    private String password;
    private String email;
    private String city;
    private int year_birth;
    private boolean isadmin;
    private String token;
    private int id;

    public User() {
    }

    public User(String username, String login, String password,
                String email, String city, int year_birth, boolean isadmin, String token) {
        this.username = username;
        this.login = login;
        this.password = password;
        this.email = email;
        this.city = city;
        this.year_birth = year_birth;
        this.isadmin = isadmin;
        this.token = token;
    }

    public User(String username, String login, String password,
                String email, String city, int year_birth, boolean isadmin, String token, int id) {
        this.username = username;
        this.login = login;
        this.password = password;
        this.email = email;
        this.city = city;
        this.year_birth = year_birth;
        this.isadmin = isadmin;
        this.token = token;
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public int getYear_birth() {
        return year_birth;
    }

    public boolean isAdmin() {
        return isadmin;
    }

    public String getToken() {
        return token;
    }

    public int getId() {
        return id;
    }
}
