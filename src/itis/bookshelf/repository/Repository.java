package itis.bookshelf.repository;

import java.util.List;

public interface Repository<T> {
    java.sql.Connection conn = itis.bookshelf.cfg.database.Connection.getConnection();

    List<T> getByQuery(String query);

}
