package itis.bookshelf.repository.impl;

import itis.bookshelf.entity.Author;
import itis.bookshelf.entity.Book;
import itis.bookshelf.entity.BookAuthor;
import itis.bookshelf.repository.interfaces.BookAuthorRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookAuthorRepositoryImpl implements BookAuthorRepository {

    public List<BookAuthor> getByQuery(String query) {
        List<BookAuthor> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new BookAuthor(
                        new Book(
                                rs.getString("book_name"),
                                rs.getString("description"),
                                rs.getString("cover_path"),
                                rs.getInt("topic_id"),
                                rs.getInt("year_pub"),
                                rs.getInt("pages"),
                                rs.getString("intro_path"),
                                rs.getString("language"),
                                rs.getBoolean("is_popular"),
                                rs.getBoolean("is_new"),
                                rs.getInt("book_id")
                        ),
                        new Author(
                                rs.getString("author_name"),
                                rs.getString("biography"),
                                rs.getString("img_path"),
                                rs.getInt("author_id")
                        )
                ));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
