package itis.bookshelf.repository.impl;

import itis.bookshelf.entity.Genre;
import itis.bookshelf.repository.interfaces.GenreRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GenreRepositoryImpl implements GenreRepository {

    public List<Genre> getByQuery(String query) {
        List<Genre> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(
                        new Genre(
                                rs.getString("name"),
                                rs.getInt("id")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

}
