package itis.bookshelf.repository.impl;

import itis.bookshelf.repository.interfaces.UserTokenRepository;
import itis.bookshelf.service.other.UpdateQuery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserTokenRepositoryImpl implements UserTokenRepository {

    public void addToken(String username, String token) {
        UpdateQuery.execute("UPDATE user_profile" + " " +
                "SET token='" + token + "' " +
                "WHERE username='" + username + "';");
    }

    public void updateToken(String username, String token) {
        addToken(username, token);
    }

    public void deleteToken(String username) {
        UpdateQuery.execute(
                "UPDATE user_profile\n" +
                        "SET token=''\n" +
                        "WHERE username='" + username + "';"
        );
    }

    public Integer findIdByToken(String token) {
        try {
            PreparedStatement st = conn.prepareStatement(
                    "SELECT id FROM user_profile WHERE token ='" + token + "'"
            );
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List getByQuery(String query) {
        return null;
    }
}
