package itis.bookshelf.repository.interfaces;

import itis.bookshelf.entity.Genre;
import itis.bookshelf.repository.Repository;

import java.util.List;

public interface GenreRepository extends Repository<Genre> {

    List<Genre> getByQuery(String query);

}
