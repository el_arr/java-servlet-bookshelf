package itis.bookshelf.repository.interfaces;

import itis.bookshelf.entity.Book;
import itis.bookshelf.repository.Repository;

import java.util.List;

public interface BookRepository extends Repository<Book> {

    List<Book> getByQuery(String query);

}
