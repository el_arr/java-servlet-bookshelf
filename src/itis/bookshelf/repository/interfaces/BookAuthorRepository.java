package itis.bookshelf.repository.interfaces;

import itis.bookshelf.entity.BookAuthor;
import itis.bookshelf.repository.Repository;

import java.util.List;

public interface BookAuthorRepository extends Repository<BookAuthor> {

    List<BookAuthor> getByQuery(String query);

}
