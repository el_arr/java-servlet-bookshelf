package itis.bookshelf.repository.interfaces;

import itis.bookshelf.entity.Review;
import itis.bookshelf.repository.Repository;

import java.util.List;

public interface ReviewRepository extends Repository<Review> {

    List<Review> getByQuery(String query);

}
