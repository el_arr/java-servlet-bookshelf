package itis.bookshelf.repository.interfaces;

import itis.bookshelf.entity.User;
import itis.bookshelf.repository.Repository;

import java.util.List;

public interface UserProfileRepository extends Repository<User> {

    void addUser(User u);

    void updateUser(User u);

    void deleteUser(User u);

    List<User> getByQuery(String query);

}
