package itis.bookshelf.repository.interfaces;

import itis.bookshelf.entity.Author;
import itis.bookshelf.repository.Repository;

import java.util.List;

public interface AuthorRepository extends Repository<Author> {

    List<Author> getByQuery(String query);

}
