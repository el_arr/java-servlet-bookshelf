package itis.bookshelf.repository.interfaces;

import itis.bookshelf.entity.BookGenre;
import itis.bookshelf.repository.Repository;

import java.util.List;

public interface BookGenreRepository extends Repository<BookGenre> {

    List<BookGenre> getByQuery(String query);

}
