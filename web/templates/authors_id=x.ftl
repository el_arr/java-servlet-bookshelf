<!DOCTYPE html>
<html lang="en">
<head>
<#include "parts/head.ftl">
    <title>${author.getName()}</title>
    <style>
        html {
            overflow-x: hidden
        }
    </style>
</head>
<body>

<#include "parts/header.ftl">

<p class="zer"></p>

<div class="row row-log">
    <div class="col-sm-2 lab">
        <img src="${author.getImg_path()}" class="book-author-img">
        <h1 style="text-align: left; font-size: 30px;">
        ${author.getName()}
        </h1>
    </div>
    <div class="col-sm-8 biog">
        <h1 style="color: black">
            БИОГРАФИЯ
        </h1>
        <p>
        ${author.getBiography()}
        </p>
    </div>
</div>

<br><br>
<h1>ЛИТЕРАТУРА</h1>

<section id="what-we-do">
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                <#include "parts/book_author.ftl">
                </div>
            </div>
        </div>
    </div>
</section>

<#include "parts/footer.ftl">

<script src="/templates/js/jquery.min.js"></script>
<script src="/templates/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>












