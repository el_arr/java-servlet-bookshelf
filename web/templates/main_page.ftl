<!DOCTYPE html>
<html lang="en">
<head>
<#include "parts/head.ftl">
    <title>Bookshelf</title>
    <link rel="stylesheet" href="/templates/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="/templates/owlcarousel/owl.theme.green.min.css">
    <link rel="stylesheet" href="/templates/owlcarousel/owl.carousel.min.css">
</head>
<body>

<#include "parts/header.ftl">

<p class="zer"></p>
<h1>ГОРЯЧИЕ НОВИНКИ</h1>
<hr color="white">

<div class="owl-carousel owl-theme">
<#list books as book>
    <#if book.isIs_new() == true>
        <div class="item">
            <a href="/book?id=${book.getId()}">
                <img src="${book.getCover_path()}">
            </a>
        </div>
    </#if>
</#list>
</div>

<hr color="white">
<br><br><br>
<h1>ПОПУЛЯРНОЕ</h1>
<hr color="white">

<section id="what-we-do">
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <#include "parts/book_author_popular.ftl">
                </div>
            </div>
            <button type="button" class="btn btn-success btn-lg"
                    style="font-size: 26px; position: absolute; left: 48%;">Еще
            </button>
        </div>
    </div>
</section>


<hr color="white">
<br><br><br>
<h1>ПОПУЛЯРНЫЕ РЕЦЕНЗИИ</h1>
<hr color="white">


<div class="recs">
    <ul>
        <li>
            <a>РЕЦЕНЗИЯ №1 НА КНИГУ ИМЯ1</a>
        </li>
        <li>
            <a>РЕЦЕНЗИЯ №2 НА КНИГУ ИМЯ2</a>
        </li>
        <li>
            <a>РЕЦЕНЗИЯ №3 НА КНИГУ ИМЯ3</a>
        </li>
        <li>
            <a>РЕЦЕНЗИЯ №4 НА КНИГУ ИМЯ4</a>
        </li>
    </ul>

</div>

<#include "parts/footer.ftl">

<script src="/templates/js/jquery.min.js"></script>
<script src="/templates/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/templates/owlcarousel/owl.carousel.min.js"></script>

<script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        smartSpeed: 1000, //Время движения слайда
        autoplayTimeout: 3000, //Время смены слайда
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 10
            }
        }
    })
</script>

</body>
</html>












