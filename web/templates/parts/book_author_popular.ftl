<div class="card-group">
<#list books_authors as ba>
    <#if ba.getBook().isIs_popular() == true>
        <div class="card">
            <div class="card-block">
                <a href="/book?id=${ba.getBook().getId()}"><h4
                        class="card-title">${ba.getBook().getName()}</h4>
                </a>
                <a href="/author?id=${ba.getAuthor().getId()}">
                    <h6 class="card-subtitle">${ba.getAuthor().getName()}</h6>
                </a>
            </div>
            <a href="/book?id=${ba.getBook().getId()}">
                <img width="80%" height="45%" src="${ba.getBook().getCover_path()}">
            </a>
        </div>
    </#if>
</#list>
</div>