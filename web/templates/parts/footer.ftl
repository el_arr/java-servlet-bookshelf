<footer id="footer-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <p>e-mail: bestBook@mail.ru</p>
            </div>

            <div class="col-sm-3">
                <a href="https://facebook.com"><img src="/templates/img/fb.png"></a>
                <a href="https://twitter.com"><img src="/templates/img/twitter.jpg"></a>
                <a href="https://vk.com"><img src="/templates/img/vk.png"></a>
            </div>

        </div>
    </div>
</footer>