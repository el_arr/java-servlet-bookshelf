<div class="card-group">
<#list books_authors_genres as bag>
    <div class="card">
        <div class="card-block">
            <a href="/book?id=${bag.getBook().getId()}"><h4
                    class="card-title">${bag.getBook().getName()}</h4>
            </a>
            <a href="/author?id=${bag.getAuthor().getId()}">
                <h6 class="card-subtitle">${bag.getAuthor().getName()}</h6>
            </a>
        </div>
        <a href="/book?id=${bag.getBook().getId()}">
            <img src="${bag.getBook().getCover_path()}">
        </a>
    </div>
</#list>
</div>