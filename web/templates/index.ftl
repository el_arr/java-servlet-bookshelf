<!DOCTYPE html>
<html lang="en">
<head>
<#include "parts/head.ftl">
    <title>Sign in</title>
    <script src="/templates/js/jquery-3.2.1.js" type="application/javascript"></script>
</head>
<body>

<section id="cover">
    <div id="cover-caption">
        <div class="container">
            <div class="col-sm-10 col-sm-offset-1">
                <output id="incorrect-data-alert"></output>
                <form action="/login" method="post" class="form-inline">
                    <div class="form-group">
                        <label class="sr-only">Username</label>
                        <input name="log_username" type="text" class="form-control form-control-lg"
                               placeholder="Имя пользователя">
                    </div>
                    <div class="form-group">
                        <label class="sr-only">Password</label>
                        <input name="log_password" type="password" class="form-control form-control-lg"
                               placeholder="Пароль">
                    </div>
                    <button name="act" value="log" id="log" type="submit" class="btn btn-success btn-lg">Войти
                    </button>
                </form>

                <br>
            </div>
        </div>
        <form>
            <button type="button" class="btn btn-success btn-lg" style="background-color: blue" data-toggle="modal"
                    data-target="#myModal">Зарегистрироваться
            </button>
        </form>
    </div>

    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h4 id="header" class="modal-title" style="color: black">Регистрация</h4>
                </div>
                <div class="modal-body" style="color: black">
                    <div class="container">
                        <div class="col-sm-10 col-sm-offset-1">

                            <form action="/login" method="post" class="form-inline">
                                <div class="form-group">
                                    <label class="sr-only">Name</label>
                                    <input name="name" type="text" class="form-control form-control-lg"
                                           placeholder="Имя">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Username</label>
                                    <input name="username" type="text" class="form-control form-control-lg"
                                           placeholder="Имя пользователя">
                                </div>


                                <script type="application/javascript" src="/templates/js/pass.js">
                                </script>

                                <div class="form-group">
                                    <label class="sr-only">Password</label>
                                    <input name="password" id="pass" type="password"
                                           class="form-control form-control-lg"
                                           placeholder="Пароль">
                                </div>
                                <label id="atten"></label>


                                <script type="application/javascript">
                                    var obj = document.getElementById('pass');
                                    obj.oninput = checkSymbols;
                                </script>


                                <div class="form-group">
                                    <label class="sr-only">Email</label>
                                    <input name="email" type="text" class="form-control form-control-lg"
                                           placeholder="Почта">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Birth year</label>
                                    <input name="year" type="text" class="form-control form-control-lg"
                                           placeholder="Год рождения">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Город</label>
                                    <input name="city" type="text" class="form-control form-control-lg"
                                           placeholder="Город">

                                </div>

                                <script src="/templates/js/login.js" type="application/javascript"></script>

                                <br/>
                                <button name="act" value="reg" id="reg" type="submit"
                                        class="btn btn-success btn-lg" onclick="trigger()">
                                    Зарегистрироваться
                                </button>

                                <script type="application/javascript">
                                    function trigger() {
                                        return alert();
                                    }
                                </script>

                            </form>

                            <br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

</section>

<!-- jQuery first, then Bootstrap JS. -->

<script src="/templates/js/jquery.min.js"></script>
<script src="/templates/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>












