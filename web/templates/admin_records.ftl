<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="styles.css">
    <style>
   html { overflow-x: hidden}
  </style>
  </head>
  <body>
      <container >
    
    
    
    
    <nav id="nav" class="navbar navbar-dark bg-inverse navbar-full" >
        <a  class="navbar-brand" href="#" href="#"><img style="padding: 0; margin: 0" src="img/logo.jpg"></a>
        <ul class="nav navbar-nav" style="font-size: 23px; padding-top: 25px">
            <li class="nav-item">
                <a class="nav-l" href="#">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-l" href="#">Книжная полка</a>
            </li>
            <li class="nav-item">
                <a class="nav-l" href="#">Каталог</a>
            </li>
            <li class="nav-item">
                <a class="nav-l" href="#">Личный кабинет</a>
            </li>
            <li class="nav-item">
                <a class="nav-l" href="#">Войти</a>
            </li>
        </ul>
              <form class="form-inline pull-xs-right">
            <input class="form-control" type="text" placeholder="Поиск">
        </form>
    </nav>
      
       <p class="zer"></p>

      
      
      
        
   
    <h1>ЛИЧНЫЙ КАБИНЕТ</h1>  
      
    <br>
      
      
      
      
       <br>
      <h1 style="text-align: left">ПАНЕЛЬ АДМИНИСТРАТОРА</h1> 
      <br>
        <br>
      <h1 style="text-align: left">БД имя_бд</h1> 
      <br>
      
     
          
          
<div class="container">
	<div class="row">
		
        
        <div class="col-md-12">
        <div class="table-responsive">

                
              <table style="color: white" id="mytable" class="table table-bordred">
                   
                   <thead>
                   <th>Book</th>
                    <th>Author</th>
                     <th>Description</th>
                     <th>Year</th>
                      <th>Edit</th>
                       <th>Delete</th>
                   </thead>
    <tbody>
    
    <tr>
    <td>Книгга</td>
    <td>Аввтор</td>
    <td>Последняя прочтенная</td>
    <td>1488</td>
    <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
        </tr>
    
    
      
    <tr>
    <td>Книгга</td>
    <td>Аввтор</td>
    <td>Последняя прочтенная</td>
    <td>1702</td>
    <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
        </tr>
    
    
</tbody>
        
</table>

<div class="clearfix"></div>
<ul class="pagination pull-right">
  <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
  <li class="active"><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
</ul>
                
            </div>
            
        </div>
	</div>
</div>
          
          
          
          
      <input type="submit" value="Выйти из режима администратора" class="btn btn-success btn-lg" style="position: relative; left: 45%; bottom: 5px;">
      
      
        
 
    <footer id="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-sm-3" >
                    <p>e-mail: bestBook@mail.ru</p>
                </div>
                
                <div class="col-sm-3" >
                        <a href=""><img src="img/fb.png"></a>
                        <a href=""><img src="img/twitter.jpg"></a>
                        <a href=""><img src="img/vk.png"></a>
                </div>
               
            </div>
        </div>
    </footer>

    <script src="/templates/js/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
          </container>
  </body>
</html>












