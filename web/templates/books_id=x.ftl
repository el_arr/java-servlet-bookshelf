<!DOCTYPE html>
<html lang="en">
<head>
<#include "parts/head.ftl">
    <title>${book_author.getBook().getName()}</title>
    <style>
        html {
            overflow-x: hidden
        }
    </style>
</head>
<body>

<#include "parts/header.ftl">

<p class="zer"></p>

<div>
    <img src="${book_author.getBook().getCover_path()}" class="book-author-img">
</div>

<div class="my-table">
    <table style="width: 100%">
        <tr>
            <th>${book_author.getBook().getName()}</th>
            <th>Скачать:</th>
            <th>Купить:</th>
        </tr>
        <tr>
            <td>${book_author.getAuthor().getName()}</td>
            <td>txt <a>skachay.ru</a></td>
            <td><a>magaz.ru</a></td>
        </tr>
        <tr>
            <td>${book_author.getBook().getYear_pub()}</td>
            <td>pdf <a>skachay.ru</a></td>
            <td><a>magaz.ru</a></td>
        </tr>
    </table>

    <table style="width: 86%">
        <tr>
            <td>
                <button class="btn btn-success btn-lg">ПРЕДПРОСМОТР</button>
            </td>
            <td>
                <button class="btn btn-success btn-lg">ОБСУЖДЕНИЕ</button>
            </td>
            <td>
                <button class="btn btn-success btn-lg">ПОСТАВИТЬ НА ПОЛКУ</button>
            </td>
            <td>
                <button class="btn btn-success btn-lg">УБРАТЬ С ПОЛКИ</button>
            </td>
        </tr>
    </table>
</div>

<div class="text-field">
    <p style="background: white; border: 4px solid darkgray;">
    ${book_author.getBook().getDescription()}
    </p>
</div>

<h1>РЕЦЕНЗИИ</h1>
<hr color="white">

<div>
<#list reviews as review>
    <div class="text-field">
        <h4>${review.getTitle()}</h4>
        <#if (review.getMark() gte 8 )>
        <p class="recen-good">
        <#elseif (review.getMark() gt 4 && review.getMark() lt 8)>
        <p class="recen-norm">
        <#elseif (review.getMark() lte 4)>
        <p class="recen-bad">
        </#if>
            ${review.getText()}
            <button class="like" type="submit"><img src="/templates/img/like.png"></button>
        </p>
    </div>
</#list>
</div>

<#include "parts/footer.ftl">

<script src="/templates/js/jquery.min.js"></script>
<script src="/templates/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>












