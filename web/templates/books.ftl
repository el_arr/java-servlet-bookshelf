<!DOCTYPE html>
<html lang="en">
<head>
<#include "parts/head.ftl">
    <title>Catalog</title>
    <style>
        html {
            overflow-x: hidden
        }
    </style>
</head>
<body>

<#include "parts/header.ftl">

<p class="zer"></p>
<form action="/books" method="get">
    <div class="row" id="filter">
        <div class="col-sm-4">
            <label>ФИЛЬТР</label>
        </div>
        <div class="col-sm-8">
            <label>Введите имя автора:</label>
            <input type="text" name="query" style="border-radius: 5%;padding: 5px; font-size: 14px;">
            <button type="submit">Искать</button>
        </div>
    </div>

<#list genreList as genres>
    <div class="row" id="filter">
        <#list genres as genre>
            <div class="col-sm-2">
                <label>${genre.getName()}</label>
                <input type="checkbox" name="genre" value="${genre.getId()}">
            </div>
        </#list>
    </div>
</#list>
</form>

<h1>
<#if queried == true>
    ПО ВАШЕМУ ЗАПРОСУ НАЙДЕНО:
<#else>
    ПОПУЛЯРНЫЕ:
</#if>
</h1>
<hr color="white">

<section id="what-we-do">
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                <#if queried == true>
                    <#include "parts/book_author_genre.ftl">
                <#else>
                    <#include "parts/book_author_popular.ftl">
                </#if>
                </div>
            </div>
        </div>
    </div>
</section>

<#include "parts/footer.ftl">

<script src="/templates/js/jquery.min.js"></script>
<script src="/templates/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>












